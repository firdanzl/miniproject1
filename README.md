TODO Front End mini project

System flow:
on Localhost:4200/ you will be taken to the landing page of the application. 
Click "GET STARTED" to be redirected to the "/login" page.
If you do not have an account, click the "Register now" button to be redirected to the "/register" page.
Once logged in, you will see the home page that includes your top 2 TO DO tasks along with a textarea on the bottom.
To start listing tasks, click the "Create a list" button to be redirected to the "/personallist" page.
Type in your task title, chose the status, and write the notes needed to complete the form, then hit the "save" button.
The tasks you save will be shown beneath the form. You can edit the task status and delete a task by clicking the "Edit task" button.
This website also comes with an "/account" page by clicking "Account" on the navigation bar.
You can check your id, email, and registered date as well as edit your profile and profile picture.

Feature mentioned:
- Create, View, Update, and Delete tasks
- Update profile
- Update profile picture


Website Screenshot:


Landing Page
![My Image](src/assets/landing-page.png)
Login
![My Image](src/assets/login.png)
Register
![My Image](src/assets/register.png)
Home
![My Image](src/assets/home.png)
Task List
![My Image](src/assets/TODO-list-task.png)
Account
![My Image](src/assets/account.png)