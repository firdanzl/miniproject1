import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserAuthService } from '../service/user-auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('',[Validators.required, Validators.email]),
    password: new FormControl('',Validators.required)
  })

  get email(){return this.form.get('email')};
  get password(){return this.form.get('password')}


  constructor(
    private userService:UserAuthService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
  
   const userLogin= {
      email:this.form.value.email??'',
      password:this.form.value.password??''
   }
    this.userService.login(userLogin).subscribe((data:any)=>{
      
      if(data){
        this.router.navigate(['home'])
        console.log(data);
        if(data.token){
         localStorage.setItem('access_token',data.token)
        }
      }
    
    })
  }

}
