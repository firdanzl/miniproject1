import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserTask } from '../service/user.interface';
import { UserTaskService } from '../service/user-task.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  myModal = document.getElementById('myModal')
  myInput = document.getElementById('myInput')

  taskData:UserTask[]=[{
    _id:'',
    title:'',
    desc:'',
    status:'',
    user:'',
    createdAt:'',
    updatedAt:'',
    __v:0
}]
  create(){
    // this.myModal?.addEventListener('shown.bs.modal',()=>{
    //   this.myInput?.focus()
    // })
    this.route.navigate(['personallist'])
  }

  viewTask(){
    const getAccessToken = String(localStorage.getItem('access_token'));
    
    this.userService.getTask(getAccessToken).subscribe((data:any)=>{
      for (let i = 0; i < 2; i++) {
        this.taskData[i] = data.data[i]
       }
       console.log(this.taskData,'taskdata')
    })
  }
  constructor(
    private route:Router,
    private userService:UserTaskService
  ) { }

  ngOnInit(): void {
    this.viewTask();
  }

}
