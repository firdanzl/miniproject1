import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PostTask } from './user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserTaskService {

  constructor(
    private http:HttpClient
  ) { }

  urlApi = 'http://localhost:3001/task'

  getTask(token:string){
    return this.http.get(this.urlApi,{
      headers:{ 'Authorization': `Bearer ${token}`}
    })
  }

  postTask(post:PostTask, token:string){
    return this.http.post(this.urlApi, post,{
      headers:{'Authorization':`Bearer ${token}`}
    })
  }

  putTask(idTask:string, status:string,token:string){
    return this.http.put(`${this.urlApi}/${idTask}`,{status},{
      headers:{'Authorization': `Bearer ${token}`}
    })
  }

  deleteTask(idTask:string, token:string){
    return this.http.delete(`${this.urlApi}/${idTask}`,{
      headers:{'Authorization':`Bearer ${token}`}
    })
  }
}
