export interface UserLogin{
    email:string,
    password:string
}

export interface UserRegister{
    name:string,
    email:string,
    password:string
}

export interface UserTask{
    _id:string,
    title:string,
    desc:string,
    status:string,
    user:string,
    createdAt:string,
    updatedAt:string,
    __v:number
}


export interface PostTask{
    title:string,
    desc:string,
    status:string
}

export interface MyUser{
    avatar:string,
    _id:string,
    name:string,
    email:string,
    role:string,
    registerAt:string,
    __v:number
}