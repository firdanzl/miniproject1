import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserLogin, UserRegister } from './user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {
 urlApi='http://localhost:3001/user'

  constructor(
    private http: HttpClient

  ) { }

  login(account:UserLogin){
    return this.http.post(`${this.urlApi}/login`,account)
  }

  register(accountRegister:UserRegister){
    return this.http.post(`${this.urlApi}/register`,accountRegister)
  }

  getUser(token:string){
    return this.http.get(`${this.urlApi}/profile`,{
      headers: {'Authorization':`Bearer ${token}`}
    })
  }

  editUser(name:string, email:string, token:string){
    return this.http.put(`${this.urlApi}/profile`,{name, email},{
      headers: {'Authorization':`Bearer ${token}`}
    })
  }
  
  updateProfPic(images:any,token:string){
  return this.http.put(`${this.urlApi}/profile/image`, images,{
    headers:{'Authorization':`Bearer ${token}`}
  })

  }

}
