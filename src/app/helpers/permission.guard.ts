import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

  constructor(
    private router:Router
  ){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log(state.url)

      const getAccessToken = localStorage.getItem('access_token');
      const activePath = state.url;

    
      if((activePath === ('/home') || activePath === '/personallist')&&(!getAccessToken)){
        this.router.navigate(['login'])
      }

      if((activePath === ('/login')|| activePath ===('/register'))&&(getAccessToken)){
        window.localStorage.clear();
      }


    return true;
  }

  logOut():void{
    window.localStorage.clear();
  }
  
}
