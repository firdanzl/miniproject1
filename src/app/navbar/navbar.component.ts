import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PermissionGuard } from '../helpers/permission.guard';
import { UserAuthService } from '../service/user-auth.service';
import { MyUser } from '../service/user.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  myUser:MyUser={
    avatar:'',
    _id:'',
    name:'',
    email:'',
    role:'',
    registerAt:'',
    __v:0
  }
  url:string=''
  constructor(
    private guard:PermissionGuard,
    private router:Router,
    private userService:UserAuthService
  ) { }

  ngOnInit(): void {
    const getAccessToken = String(localStorage.getItem('access_token'))
    this.userService.getUser(getAccessToken).subscribe((data:any)=>{
      this.myUser = data.data
      this.url=this.myUser.avatar
      console.log(this.myUser);
    })
   
  }
  
  logout(){
    this.guard.logOut();
    if(!localStorage.getItem('access_token')){
      this.router.navigate(['login'])
    }
  }

}
