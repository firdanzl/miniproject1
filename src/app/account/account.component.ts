import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { timer } from 'rxjs';
import { UserAuthService } from '../service/user-auth.service';
import { MyUser } from '../service/user.interface';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  myUser:MyUser={
    avatar:'',
    _id:'',
    name:'',
    email:'',
    role:'',
    registerAt:'',
    __v:0
  }
  url:string=''

  formEdit = new FormGroup({
    name : new FormControl(''),
    email : new FormControl('')
  })
  constructor(
    private userService :UserAuthService
  ) { }

  ngOnInit(): void {
    this.viewUser();
  }

  viewUser(){
    const getAccessToken = String(localStorage.getItem('access_token'))
    this.userService.getUser(getAccessToken).subscribe((data:any)=>{
      this.myUser = data.data
      this.url=this.myUser.avatar
      console.log(this.myUser.avatar)
      this.myUser.registerAt = this.myUser.registerAt.slice(0,10)
      console.log(this.myUser);
    })

  
  }

  updateProfile(){
    const getAccessToken = String(localStorage.getItem('access_token'))
    const name= this.formEdit.value.name??''
    const email= this.formEdit.value.email??''
    this.userService.editUser(name,email,getAccessToken).subscribe((data:any)=>{
      console.log(data);
      
    })
    timer(1500).subscribe(()=>{
      this.viewUser()
    })
  }

  editUser(){
    this.formEdit.patchValue(this.myUser)
  }
  


  onSelectFile(event:any) {
    const getAccessToken = String(localStorage.getItem('access_token'))
    if (event.target.files && event.target.files[0]) {
      const formData = new FormData()
      formData.append('images',event.target.files[0])
      this.userService.updateProfPic(formData,getAccessToken).subscribe((data:any)=>{
        console.log(data);
        
        this.viewUser()
      })
      
      //var reader = new FileReader();

     // reader.readAsDataURL(event.target.files[0]); // read file as data url

     // reader.onload = (event:any) => { // called once readAsDataURL is completed
       // this.url = event.target.result;
      //}
    }
   
  }

}
