import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { timer } from 'rxjs';
import { UserTaskService } from '../service/user-task.service';
import { UserTask } from '../service/user.interface';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {
  getAccessToken:string=''

  check:UserTask[]=[{
    _id:'',
    title:'',
    desc:'',
    status:'',
    user:'',
    createdAt:'',
    updatedAt:'',
    __v:0
}]
  //title:string[]=[];
  idIndex:number=0
  checkboxChecked:boolean=false
  taskData:UserTask[]=[{
    _id:'',
    title:'',
    desc:'',
    status:'',
    user:'',
    createdAt:'',
    updatedAt:'',
    __v:0
}]

  form = new FormGroup ({
    title: new FormControl(''),
    desc: new FormControl(''),
    status: new FormControl('')
  })

  formEdit = new FormGroup({
    title: new FormControl(''),
    status: new FormControl('')
  })

  constructor(
    private userService:UserTaskService,
   
  ) { }

  ngOnInit(): void {
    this.viewtask()
    
  }

  viewtask(){
    const getAccessToken = String(localStorage.getItem('access_token'));
    console.log(getAccessToken);
    
    this.userService.getTask(getAccessToken).subscribe((data:any)=>{
       //console.log(data.data);
       this.taskData=[{
        _id:'',
        title:'',
        desc:'',
        status:'',
        user:'',
        createdAt:'',
        updatedAt:'',
        __v:0
       }]
       for (let i = 0; i < data.data.length; i++) {
        this.taskData[i] = data.data[i]

        if (data.data[i].createdAt === this.taskData[i].createdAt){
          this.taskData[i].createdAt = this.taskData[i].createdAt.slice(0,10)
          this.taskData[i].updatedAt = this.taskData[i].updatedAt.slice(0,10)
        }
       }
     
       //console.log(this.taskData,'taskdata')
     })
   }

  saveTask(){
    const getAccessToken = String(localStorage.getItem('access_token'));
   
    const postTask = {
      title:this.form.value.title??'',
      desc:this.form.value.desc??'',
      status:this.form.value.status??''
    }
    
    this.userService.postTask(postTask,getAccessToken).subscribe((data:any)=>{
      console.log(data);
      this.viewtask();
      this.form.patchValue({
        title:'',
        desc:'',
        status:''
      })
    })
    
  }

  createModal(item:any){
      // this.myModal?.addEventListener('shown.bs.modal',()=>{
    //   this.myInput?.focus()
    // })
    
    this.check.push(item)
    this.findIndexforEdit(item._id)
    //console.log(this.idIndex)
    this.formEdit.patchValue(this.check[this.idIndex])
    
   // console.log(this.check[this.idIndex].title)
    console.log(this.idIndex)
  }

  updateTask(idUpdate:string){
    const getAccessToken = String(localStorage.getItem('access_token'));
    const status = this.formEdit.value.status??''
    console.log(idUpdate);
    
    this.userService.putTask(idUpdate,status,getAccessToken).subscribe((data:any)=>{
      console.log(data);
      
    })
    timer(2000).subscribe(()=>{
      this.viewtask(); 
    })
    
  }

  deleteTask(idDelete:string){
    const getAccessToken = String(localStorage.getItem('access_token'));
    //const status = this.formEdit.value.status??''

    this.userService.deleteTask(idDelete,getAccessToken).subscribe((data:any)=>{
      console.log(data)
    })
    timer(2000).subscribe(()=>{
      this.viewtask(); 
    })
  }

  findIndex(needId:string){
    this.idIndex = this.taskData.findIndex((x:any)=>{return x._id===needId});
  }

  findIndexforEdit(needId:string){
    this.idIndex = this.check.findIndex((x:any)=>{return x._id===needId});
    //console.log(needId)
  }

  }
