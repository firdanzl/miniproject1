import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { PermissionGuard } from './helpers/permission.guard';
import { HomeComponent } from './home/home.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './login/login.component';
import { PersonalComponent } from './personal/personal.component';
import { RegisterComponent } from './register/register.component';
import { WorkComponent } from './work/work.component';

const routes: Routes = [
  {path:'',component:LandingPageComponent},
  {path:'home',component:HomeComponent, canActivate:[PermissionGuard]},
  {path:'login',component:LoginComponent, canActivate:[PermissionGuard]},
  {path:'register',component:RegisterComponent, canActivate:[PermissionGuard]},
  {path:'worklist',component:WorkComponent},
  {path:'personallist',component:PersonalComponent, canActivate:[PermissionGuard]},
  {path:'account', component:AccountComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
