import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserAuthService } from '../service/user-auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('',[Validators.required, Validators.email]),
    password: new FormControl('',[Validators.required, Validators.minLength(8)]),
    confirmPassword: new FormControl('',Validators.required)
  })

  get name(){return this.form.get('name')};
  get email(){ return this.form.get('email')};
  get password(){ return this.form.get('password')};
  get confirmPassword(){ return this.form.get('confirmPassword')}

  constructor(
    private userService:UserAuthService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    //console.log(this.form.value);
    //const{name, email, password} = this.form.value
    const userRegister= {
      name:this.form.value.name??'',
      email:this.form.value.email??'',
      password:this.form.value.password??'',
      //role : 'member'
   }
    this.userService.register(userRegister).subscribe((data:any)=>{
      console.log(data);
      
    })
    this.router.navigate(['login'])
  }
}
